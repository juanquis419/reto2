package geometrias;

public class Circulo extends Figura {

    private double radio;

    public void asignarRadio(double valRadio){

        this.radio = valRadio;
    }    

    public double consultarRadio(){

        return this.radio;
    }  

    public double area() {

        return Math.PI * Math.pow(this.radio, 2);
    }

    public double perimetro() {

        return 2 * Math.PI * this.radio;
    }

    public Circulo(String elNombre, double elRadio) {
        super(0, "Circulo");

        this.radio = elRadio;

    }

    @Override    
    public String toString (){

        return "\n- La figura " + consultarNombre() + " tiene " + consultarLados() + " lados, cuya área es " + String.format("%,.2f", area()) +" und y su perímetro es " + String.format("%,.2f", perimetro()) + " und\n";

    }
    
}
