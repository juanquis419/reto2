package geometrias;

public class Triangulo extends Figura {

    private double base;
    private double altura;
    private double lgtdLado1;
    private double lgtdLado2;
    private double lgtdLado3;
 
    public void asignarBase(double valBase){

        this.base = valBase;
    }    

    public double consultarBase(){

        return this.base;
    }

    public void asignarAltura(double valAltura){

        this.altura = valAltura;
    }    

    public double consultarAltura(){

        return this.altura;
    }

    public void asignarlgtdLado1(double valLgtdLado1){

        this.lgtdLado1 = valLgtdLado1;
    }    

    public double consultarLgtdLado1(){

        return this.lgtdLado1;
    }

    public void asignarlgtdLado2(double valLgtdLado2){

        this.lgtdLado2 = valLgtdLado2;
    }    

    public double consultarLgtdLado2(){

        return this.lgtdLado2;
    }

    public void asignarlgtdLado3(double valLgtdLado3){

        this.lgtdLado3 = valLgtdLado3;
    }    

    public double consultarLgtdLado3(){

        return this.lgtdLado3;
    }

    public double area() {

        return (this.base * this.altura) / 2 ;

    }

    public double perimetro() {

        return this.lgtdLado1 + this.lgtdLado2 + this.lgtdLado3;

    }

    public Triangulo(String elNombre, double laBase, double laAltura, double longitud1, double longitud2, double longitud3) {
        super(3, elNombre);
        
        this.base       = laBase;
        this.altura     = laAltura;
        this.lgtdLado1  = longitud1;
        this.lgtdLado2  = longitud2;
        this.lgtdLado3  = longitud3;
    }

    @Override    
    public String toString (){

        return "\n- La figura " + consultarNombre() + " tiene " + consultarLados() + " lados, cuya área es " + String.format("%,.2f", area()) +" und y su perímetro es " + String.format("%,.2f", perimetro()) + " und\n";

    }    
    
}
