package geometrias;

public class Figura {

    private int lados;
    private String nombre;

    // Juan Camilo Avendaño Miranda

    public void asignarLados(int numLados){

        this.lados = numLados;
    }    

    public int consultarLados(){

        return this.lados;
    }  
    
    public void asignarNombre(String newNombre){

        this.nombre = newNombre;
    }   

    public String consultarNombre(){
        return this.nombre;
    }   

    public Figura(int numLados, String elNombre) {

        this.lados = numLados;
        this.nombre = elNombre;
    }

    public String toString (){

        return "La figura " + this.nombre + " tiene " + this.lados + " lados";

    }
   
}
