package geometrias;

public class Cuadrado extends Figura {
    
    private double longitud;

    public void asignarLongitud(double valLongitud){

        this.longitud = valLongitud;
    }    

    public double asignarLongitud(){

        return this.longitud;
    }  

    public double perimetro() {

        return this.longitud * consultarLados() ;

    }

    public double area() {

        return this.longitud * this.longitud ;

    }

    public Cuadrado(String elNombre, double laLongitud) {
        super(4, elNombre);
        
        this.longitud = laLongitud;
    }

    @Override    
    public String toString (){

        return "\n- La figura " + consultarNombre() + " tiene " + consultarLados() + " lados, cuya área es " + String.format("%,.2f", area()) +" und y su perímetro es " + String.format("%,.2f", perimetro()) + " und\n";

    }
    
}
