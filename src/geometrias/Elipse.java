package geometrias;

public class Elipse extends Figura {

    private double radioMenor;
    private double radioMayor;

    public void asignarRadioMenor(double valRadioMenor){

        this.radioMenor = valRadioMenor;
    }    

    public double consultarRadioMenor(){

        return this.radioMenor;
    }  

    public void asignarRadioMayor(int valRadioMayor){

        this.radioMayor = valRadioMayor;
    }    

    public double consultarRadioMayor(){

        return this.radioMayor;
    }  

    public double area() {

        return Math.PI * this.radioMenor * this.radioMayor;
    }

    public double perimetro() {

        return 2 * Math.PI * Math.sqrt((Math.pow(this.radioMenor, 2) + Math.pow(this.radioMayor, 2)) / 2);
    }

    public Elipse(String elNombre, double elRadioMenor, double elRadioMayor) {
        super(0, "Elipse");

        this.radioMenor = elRadioMenor;
        this.radioMayor = elRadioMayor;

    }

    @Override    
    public String toString (){

        return "\n- La figura " + consultarNombre() + " tiene " + consultarLados() + " lados, cuya área es " + String.format("%,.2f", area()) +" und y su perímetro es " + String.format("%,.2f", perimetro()) + " und\n";

    }
    
}
