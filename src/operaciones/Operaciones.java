package operaciones;

import geometrias.*;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

public class Operaciones {

    public Double inputParam(String figura) {

        /* Función inputUserNumDbl:

           - Descripción: Función que se encarga de solicitar al usuario un input double
                        
           - Inputs: figura -> Tipo String

           - Output: returna el input del usuario -> Tipo entero 
        */
        
        Scanner input = new Scanner(System.in);
        
        Double numero = null;

        while(numero == null) {

            System.out.println("Ingrese el parametro " + figura + " de la figura");
            
            String auxInput = input.nextLine();

            try{

                numero = Double.parseDouble(auxInput);
                
            } catch (Exception e) {

                System.out.println("El input falló en el input del número, ya que no tiene el formato correcto, vuelva a intentarlo");
                erroLogFile(e.toString());
                
            }

        }

        return numero;
    }

    public void erroLogFile(String err){

        /* Función erroLogFile:

           - Descripción: Función que se encarga de almacenar los log de los errores de ejecución que se tienen al momento
                          de ejecutar el programa
                        
           - Inputs: err (error) -> Tipo String
        */

        Logger logger = Logger.getLogger("MyLog");  
        FileHandler fh;  
        
        try {
            String ruta = System.getProperty("user.dir") + "\\errorLog.log";

            fh = new FileHandler(ruta, true);  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  
  
            logger.info(err);  

        } catch (Exception e) {

            System.out.println("Ocurrió un error");
        }

    }

    public void main() {

        Operaciones oper    = new Operaciones();
        Boolean continuar   = true;
        Double valPerimetro;
        Double valArea;
        
        while (continuar) {

            Scanner input = new Scanner(System.in);

            System.out.println("Seleccione uno de los números para generar los valores de la figura deseada");
            System.out.println("- 1.Cuadrado");
            System.out.println("- 2.Triangulo");
            System.out.println("- 3.Circulo");
            System.out.println("- 4.Elipse");
            System.out.println("- 5.Salir");

            String respuesta = input.nextLine().trim();

            if (respuesta.equals("1")) {

                // Cuadrado 

                double param = oper.inputParam("Longitud");

                Cuadrado sqr = new Cuadrado("Cuadrado", param);

                valArea = sqr.area();
        
                valPerimetro = sqr.perimetro();
        
                System.out.println(sqr);

            } else if (respuesta.equals("2")) {
                
                // Triangulo 
                
                String [] parametros    = {"Base", "Altura", "Longitud 1", "Longitud 2", "Longitud 3"};
                Double [] valParametros = new Double[parametros.length];
        
                for (int pos = 0; pos < parametros.length; pos++) {

                    valParametros[pos] = oper.inputParam(parametros[pos]);

                }

                Triangulo trg = new Triangulo("Triangulo", valParametros[0], valParametros[1], valParametros[2], valParametros[3], valParametros[4]);

                valArea = trg.area();

                valPerimetro = trg.perimetro();

                System.out.println(trg);

            } else if (respuesta.equals("3")) {

                // Circulo

                double param = oper.inputParam("Radio");

                Circulo crl = new Circulo("Circulo", param);

                valArea = crl.area();

                valPerimetro = crl.perimetro();

                System.out.println(crl);

            } else if (respuesta.equals("4")) {

                // Elipse

                String [] parametros    = {"Base menor", "Base mayor"};
                Double [] valParametros = new Double[parametros.length];
        
                for (int pos = 0; pos < parametros.length; pos++) {

                    valParametros[pos] = oper.inputParam(parametros[pos]);

                }

                Elipse elp = new Elipse("Elipse", valParametros[0], valParametros[1]);

                valArea = elp.area();

                valPerimetro = elp.perimetro();

                System.out.println(elp);

            } else if (respuesta.equals("5")) {

                System.out.println("\nHasta luego!\n");
                continuar = false;

            } else {

                System.out.println("Input invalido ingrese un valor correcto!");
                
            }

        }

    }
    
}
