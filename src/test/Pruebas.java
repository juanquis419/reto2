package test;

import geometrias.*;

import org.junit.Assert;
import org.junit.Test;

public class Pruebas {

    @Test
    public void pruebaCuadrado(){
        Cuadrado square     = new Cuadrado("Cuadrado", 10);
        Double   rtaArea    = square.area();
        Double   rtaPerim   = square.perimetro();
        Double   valArea    = 100D;
        Double   valPerim   = 40D;
        
        Assert.assertEquals(valArea, rtaArea);

        Assert.assertEquals(valPerim, rtaPerim);
    }

    
}
